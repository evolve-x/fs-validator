/**
 * @license
 *
 * Evolve-X FS-Validator validates the Database and File System, and removes errors if wanted.
 * Copyright (C) 2019 VoidNulll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * @author VoidNulll
 * @version 1.0.0
 */

import { Schema, model, Model, Document } from 'mongoose';

const Image: Schema = new Schema( {
    ID: { type: String, required: true },
    owner: { type: String, required: true },
    path: { type: String, required: true },
    type: { type: String, required: false },
} );
/* eslint-disable */
export interface UploadI extends Document {
    ID: string;
    owner: string;
    format: string;
    path: string;
    type?: string;
}

const mod: Model<UploadI> = model<UploadI>('image', Image);
/* eslint-enable */
export default mod;