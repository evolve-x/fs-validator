# fs-validator

Validates the Database and File System for an Evolve-X instance (files only).

# Config

`path` - This is just the path to Evolve-X, the app will handle everything from there.

# Note

Any destroyed data is not to be fault of VoidNulll or Contributors.

This app will delete data, be careful.

This **IS** a destructive app, it is designed to delete files and database documents.

**THE SOFTWARE IS TESTED WITH EVOLVE-X VERSION 1.4.2 AND IS NOT GUARANTEED TO WORK WITH PREVIOUS OR LATER VERSIONS (As of December 31st, 2019)**

# WARRANTY

**THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.**

# Copyright

Copyright (c) VoidNulll 2019