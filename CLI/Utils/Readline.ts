/**
 * @license
 *
 * Evolve-X FS-Validator validates the Database and File System, and removes errors if wanted.
 * Copyright (C) 2019 VoidNulll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * @author VoidNulll
 * @version 1.0.0
 */

import * as readline from 'readline-sync';

export default class RL {
    constructor() {
    }

    public question(query: string, answerOptions?: string[]) {
        let q = `${query}\n`;
        if (answerOptions) {
            q = `${query} (${answerOptions.join('|')})\n`;
            return Promise.resolve(readline.question(q, { limit: answerOptions, limitMessage: `"$<lastInput>" is not a valid option. Choose from these: ${answerOptions.join(', ')}` } ) );
        } else {
            return Promise.resolve(readline.question(q));
        }
    }
}