/**
 * @license
 *
 * Evolve-X FS-Validator validates the Database and File System, and removes errors if wanted.
 * Copyright (C) 2019 VoidNulll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * @author VoidNulll
 * @version 1.0.0
 */

import * as fs from 'fs-extra';
import { join } from "path";
import * as superagent from 'superagent';
import Image, {UploadI} from '../Schemas/Image';
import Checker from './Checker';

export default class Remove {

    private checker: Checker;

    constructor() {
        this.checker = new Checker({ remove: true } );
    }

    async deleteFiles(files: string[]): Promise<number> {
        let removed = 0;
        for(let file of files) {
            if (fs.existsSync(file) ) {
                console.log(`Deleting file ${file}`);
                await fs.remove(file);
                console.log(`Deleted file ${file}`);
                removed++;
            }
        }
        return removed;
    }

    async deleteDocuments(docs: UploadI[]): Promise<{ removed: number; existed: number }> {
        let removed = 0;
        let existed = 0;
        for (let document of docs) {
            if (!fs.existsSync(document.path) ) {
                console.log(`Deleting document ${document.ID} belonging to ${document.owner}`);
                await Image.deleteOne({ ID: document.ID } );
                console.log(`Deleted document ${document.ID} belonging to ${document.owner} with path ${document.path}`);
                removed++;
            } else {
                existed++;
            }
        }
        return { removed, existed };
    }

    async start() {
        const out = await this.checker.start();
        if (!out) {
            return process.exit(0);
        }
        if (!out.failedDB.length && !out.failedFiles.length) {
            console.log('[OK] Nothing to remove.');
            process.exit(0);
        }
        let str = '';
        let altstr = '';
        if (out.failedFiles.length > 0) {
            str = `Would you like to remove ${out.failedFiles.length} files`;
            altstr = `${out.failedFiles.length} files`;
        }
        if (out.failedDB.length > 0) {
            if (str.length > 0) {
                str += ` and ${out.failedDB.length} Database File Documents`;
                altstr += ` and ${out.failedDB.length} Database File Documents`;
            } else {
                str = `Would you like to remove ${out.failedDB.length} Database File Documents`;
                altstr = `${out.failedDB.length} Database File Documents`;
            }
        }
        str += ` from this Evolve-X Instance located at ${this.checker.path}?`;
        const answer = await this.checker.rl.question(str, ['yes', 'y', 'no', 'n']);
        if (['no', 'n'].includes(answer.toLowerCase() ) ) {
            console.log('OK. Goodbye!');
            process.exit(0);
        } else if (['yes', 'y'].includes(answer.toLowerCase() ) ) {
            const nanswer = await this.checker.rl.question('Are you sure? This action is irreversible.', ['yes', 'y', 'no', 'n']);
            if (['no', 'n'].includes(nanswer.toLowerCase() ) ) {
                console.log('OK. Goodbye!');
                process.exit(0);
            } else {
                try {
                    await superagent(`localhost:${this.checker.conf.port}/api`);
                    console.log('\n[FATAL] Evolve-X must not be online during verification!');
                    process.exit(0);
                } catch (e) {
                    if (e.errno && e.errno === 'ECONNREFUSED') {
                        //
                    }
                }
                console.log(`Proceeding with deletion of ${altstr}!`);
                let str = '';
                if (out.failedFiles.length > 0) {
                    console.log('Deleting files...');
                    const delFiles = await this.deleteFiles(out.failedFiles);
                    if (delFiles && delFiles > 0) {
                        str = `Deleted ${delFiles} files`;
                    }
                }
                if (out.failedDB.length > 0) {
                    console.log('Deleting documents...');
                    const delDocuments = await this.deleteDocuments(out.failedDB);
                    if (delDocuments) {
                        if (str.length > 0) {
                            str += delDocuments.removed && delDocuments.removed > 0 ? `, removed ${delDocuments.removed} documents` : '';
                            str += delDocuments.existed && delDocuments.existed > 0 ? `, and could not delete ${delDocuments.existed} due to having paths associated with them.` : '';
                        } else {
                            const noRemoved = !delDocuments.removed && delDocuments.existed === out.failedDB.length ? 'No documents removed, all had paths associated.' : undefined;
                            str = noRemoved || `${delDocuments.existed > 0 ? `Removed ${delDocuments.removed}/${out.failedDB.length} documents` : 'Removed all documents'}`;
                        }
                    }
                }
                console.log(str);
                console.log('\nRemoving finished, goodbye.');
                process.exit(0);
            }
        } else {
            console.log('What? I did not catch that.\n\nYou will have to restart the process to make a decision.');
            process.exit(0);
        }
    }
}