/**
 * @license
 *
 * Evolve-X FS-Validator validates the Database and File System, and removes errors if wanted.
 * Copyright (C) 2019 VoidNulll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * @author VoidNulll
 * @version 1.0.0
 */

import RL from '../Utils/Readline';
import * as fs from 'fs-extra';
import * as config from "../../config.json";
import { join } from "path";
import * as superagent from 'superagent';
import *as mongoose from "mongoose";
import Image, {UploadI} from '../Schemas/Image';
import EvolveConfig from "./Evolve-Config";

interface ValidReturn {
    imgs: string[];
    okFiles: number;
    okDB: number;
    failedFiles: string[];
    failedDB: UploadI[]
}

export default class Checker {

    public rl: RL;

    public path: string;

    public images: string;

    private remove: boolean;

    public conf: any;

    public args?: string[];

    constructor(obj?: { remove?: boolean, args?: string[] }) {
        this.remove = obj && obj.remove || false;
        this.args = obj && obj.args || [];
        this.rl = new RL();

        if (!config.path) {
            console.log('[FATAL] No Evolve-X path was specified.');
            process.exit();
        } else {
            console.log('Config check OK\n');
        }
        this.path = config.path.startsWith('../') ? join(__dirname, '../../', config.path) : config.path;

        if (!fs.pathExistsSync(this.path) ) {
            console.log('[FATAL] Path does not exist');
            process.exit();
        } else {
            console.log('Directory check OK\n');
            console.log(`Evolve-X Directory: ${this.path}`);
        }

        this.images = join(this.path, 'src/Images');

        if (!fs.existsSync(this.images) ) {
            console.log('[FATAL - FS] Images directory does not exist.');
            process.exit();
        } else {
            console.log('\nImages directory check OK\n');
            console.log(`Images Directory: ${this.images}`);
            console.log('')
        }

        const EVConfFile = require(join(this.path, '/config.json') );
        this.conf = new EvolveConfig(EVConfFile);
    }

    async validate(): Promise<ValidReturn> {
        const imgs = await fs.readdir(this.images);
        console.log(`Going through ${imgs.length} files. This could take a bit.`);
        let safe = 0;
        const images = await Image.find();
        const length = imgs.length - 1;
        const failed = [];
        for (const img of imgs) {
            if (img !== '.gitkeep') {
                const im = images.find(a => a.path.replace(/\\/g, '/') === join(this.images, img).replace(/\\/g, '/') );
                if (im) {
                    safe++;
                } else {
                    console.log(`\nFAIL: ${join(this.images, img)}\n`);
                    failed.push(join(this.images, img));
                }
            }
        }
        let safeDB = 0;
        const failedDB = [];
        const npath = imgs.map(m => join(this.images, m).replace(/\\/g, '/') );
        for (const img of images) {
            const path = img.path.replace(/\\/g, '/');
            const im = npath.find(p => p === path);
            if (im) {
                safeDB++;
            } else {
                console.log(`\nDB FAIL:\n-- ID: ${img.ID}\n-- Owner: ${img.owner}\n-- Path: ${img.path.replace(/\\/g, '\\')}`);
                failedDB.push(img);
            }
        }
        console.log(`\n\nData Loss (Files): ${((length - safe)/length)*100}`);
        console.log(`OK File Count: ${safe}/${length}`);
        console.log(`\nData Loss (DB): ${((images.length - safeDB)/images.length)*100}`);
        console.log(`OK DB Documents: ${safeDB}/${images.length}\n\n`);
        return { imgs, okFiles: safe, okDB: safeDB, failedFiles: failed, failedDB };
    }

    async start(): Promise<void|ValidReturn> {
        try {
            await superagent(`localhost:${this.conf.port}/api`);
            console.log('\n[FATAL] Evolve-X must not be online during verification!');
            process.exit(0);
        } catch (e) {
            if (e.errno && e.errno === 'ECONNREFUSED') {
                //
            }
        }
        const out = !this.args || !['y', 'yes'].includes(this.args[0]) ? await this.rl.question('Would you like to proceed with checking?', ['yes', 'y', 'no', 'n'] ) : 'y';
        if (['no', 'n'].includes(out.toLowerCase() ) ) {
            console.log('Farewell then.');
            process.exit();
        } else {
            try {
                await mongoose.connect(this.conf.mongoUrl, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true } );
            } catch (e) {
                console.log('\n[FATAL] Database connection fail. Cannot continue.');
                process.exit(0);
            }
            try {
                await superagent(`localhost:${this.conf.port}/api`);
                console.log('\n[FATAL] Evolve-X must not be online during verification!');
                process.exit(0);
            } catch (e) {
                if (e.errno && e.errno === 'ECONNREFUSED') {
                    //
                }
            }

            const out = await this.validate();
            console.log('Checking finished');
            if (!this.remove) {
                setTimeout(() => {
                    process.exit(0)
                }, 1000);
            } else {
                return out;
            }
        }
    }
}
