#!/usr/bin/env node

import {symlink} from "fs-extra";

const { platform, userInfo } = require('os');
const { readFile, existsSync, ensureSymlink, mkdir, copy, chmod } = require('fs-extra');
const { writeFileSync } = require('fs');

if (platform() !== 'linux') {
    console.log('[FATAL] Evolve-X FS-Validator CLI is only designed to run in Linux');
}


let cliPath = __dirname;
let execPath = __dirname;
if (__dirname.match(/bin/) ) {
    const user = userInfo();
    const conf = `${user.homedir}/.efsv/config.json`;
    if (existsSync(conf) ) {
        const aConf = require(conf);
        cliPath = aConf.path;
    } else {
        console.log('Not configured!');
        process.exit();
    }
}

const [,, ...args] = process.argv;

const { promisify } = require('util');
const cp = require('child_process');
const exec = promisify(cp.exec);
const { join } = require('path');

function getVersion() {
    if (!existsSync(join(cliPath, './versions.json') ) ) {
        return 'Version 1.0';
    } else {
        const { cli } = require(join(cliPath, './versions.json'));
        return `Version ${cli}`;
    }
}

if (!args[0] ) {
    console.log(`\nEvolve-X FS-Validator CLI. ${getVersion()}\n`);
    console.log('Usage: [command]');
    console.log('\n  [COMMANDS]:\n    --verify (args) - Show diagnosis of the Evolve-X database and file system\n    --remove (args) - Show diagnosis and allow to remove files and database documents that have no partner.\n    -v|--version - Display the version\n    -u|--update - Update the CLI and software\n    --makecmd (path for symlink) - Makes efsv into a callable command (uses ~/bin or path to create a symlink)\n    --config [path]\n\n  [ARGUMENTS]:\n    -y|--yes - Force the prompts to take the answer yes. Overrides ALL prompts.\n\n  [WARNING] This is destructive of data. No warranty is provided.\n');
    console.log('\nCopyright (c) 2019 VoidNulll\nLicense: AGPLv3 (https://gitlab.com/evolve-x/fs-validator/blob/master/LICENSE)\n');
    process.exit(0);
}


function getArgs() {
    const aArgs = args.slice(1).join(' ').split(/--|-/);
    let argz = [];
    for(let arg of aArgs) {
        argz.push(arg.trim());
    }
    return argz;
}

const command = args[0];
const aArgs = getArgs();

function getMainVersion() {
    try {
        return require(join(cliPath, './versions.json')).main;
    } catch (e) {
        return require(join(cliPath, './package.json')).version;
    }
}

(async function handler() {
    if (['-v', '--version'].includes(command) ) {
        const version = getVersion();
        console.log(`\n${version}\n`);
        process.exit();
    } else if (command === '--verify') {
        const Verify = require(join(cliPath, './CLI/Structures/Checker')).default;
        let args = [];
        const arg = aArgs.find(arg => arg === 'y' || arg === 'yes');
        if (arg) {
            args.push(arg)
        }
        const verify = new Verify({ args });
        await verify.start();
        process.exit();
    } else if (command === '--remove') {
        const Remove = require(join(cliPath, './CLI/Structures/Remove')).default;
        let args = [];
        const arg = aArgs.find(arg => arg === 'y' || arg === 'yes');
        if (arg) {
            args.push(arg)
        }
        const remove = new Remove({ args });
        await remove.start();
        process.exit();
    } else if(command === '--update' || command === '-u') {
        const v = getMainVersion();
        const cliv = getVersion().slice('version '.length);
        console.log('This will take a while...');
        console.log('\nPulling with GIT\n');
        try {
            const out = await exec(`cd ${cliPath} && git pull`);
            console.log(out.stdout);
            if (out.stdout.match('Already up to date.') ) {
                console.log('\n\nNo changes!\n');
                process.exit(0);
            }
        } catch (e) {
            console.log(`Error while updating GIT\n${e}`);
            process.exit(1);
        }
        const f = await readFile(join(cliPath, './versions.json'));
        const af = JSON.parse(f.toString() );
        console.log('\nRunning TypeScript Compiler on main directory\n');
        try {
            await exec(`cd ${cliPath} && tsc`);
        } catch (e) {
            console.log('Error:');
            console.log(e);
            process.exit();
        }
        console.log('\nUpdating CLI (TypeScript)\n');
        try {
            await exec(`cd ${join(cliPath, '/CLI')} && tsc`);
        } catch (e) {
            console.log('Error:');
            console.log(e);
            process.exit();
        }

        console.log(`\nCLI Version: v${cliv} => v${af.cli}`);
        console.log(`\nMain version: v${v} => v${af.main}`);
        console.log('\nUpdate seems to have gone smoothly!\n');
    } else if(command === '--makecmd') {
        if (__dirname.match('bin') ) {
            console.log('Already a command!');
            process.exit();
        }
        let str: string|string[] = execPath;
        str = str.split('/');
        str.shift();
        if (!existsSync(`/home/${str[1]}/.efsv`) ) {
            await mkdir(`/home/${str[1]}/.efsv`);
        }
        if (!existsSync(`/home/${str[1]}/.efsv/config.json`) ) {
            const config = { path: __dirname };
            writeFileSync(`/home/${str[1]}/.efsv/config.json`, JSON.stringify(config) );
        }
        const dir = args;
        dir.shift();
        const aDir = dir[0] || `${userInfo().homedir}/bin`;
        if (!existsSync(aDir) ) {
            await mkdir(aDir);
        }
        await ensureSymlink(join(execPath, 'cli.js'), `${aDir}/efsv`);
        console.log(`Symlink: ${aDir}/efsv => ${join(execPath, 'cli.js')}\nDirectories and files created!\n\nAdd this to your PATH (if not present):\nexport PATH=$PATH":${aDir}"`);
    } else if (command === '--config') {
        const dir = args.slice(1);
        if (!dir || !existsSync(dir) ) {
            console.log('Not a directory!');
            process.exit();
        }
        const conf = { path: dir };
        const user = userInfo();
        if (!existsSync(`${user.homedir}/.efsv`) ) {
            await mkdir(`${user.homedir}/.efsv`);
        }
        writeFileSync(`${user.homedir}/./efsv/config.json`, JSON.stringify(conf) );
        console.log(`Set configuration path to ${dir}`);
        process.exit();
    } else {
        console.log(`\nEvolve-X FS-Validator CLI. ${getVersion()}\n`);
        console.log('Usage: [command]');
        console.log('\n  [COMMANDS]:\n    --verify (args) - Show diagnosis of the Evolve-X database and file system\n    --remove (args) - Show diagnosis and allow to remove files and database documents that have no partner.\n    -v|--version - Display the version\n    -u|--update - Update the CLI and software\n    --makecmd - Makes efsv into a callable command (uses ~/bin)\n    --config [path]\n\n  [ARGUMENTS]:\n    -y|--yes - Force the prompts to take the answer yes. Overrides ALL prompts.\n\n  [WARNING] This is destructive of data. No warranty is provided.\n');
        console.log('\nCopyright (c) 2019 VoidNulll\nLicense: AGPLv3 (https://gitlab.com/evolve-x/fs-validator/blob/master/LICENSE)\n');
        process.exit(0);
    }
}());

